# README #

Projeto de Gestão Clinica

### Requisitos ###

* MongoDB
* PostgreSQL
* Ruby
* Node

### Setup ###

* rake db:migrate
* rake db:reset

### Auth ###

* Email: demo@localhost.com
* Password: 123456789
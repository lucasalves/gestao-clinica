require 'rails_helper'

RSpec.describe Statistics::Filter, type: :model do
  context 'usando o período total' do
    it 'não filtra pela data' do
      filter = Statistics::Filter.build({
        'period' => 'all'
      })

      where = {
        :current => {},
        :previous => {}
      }

      expect(filter).to eq(where)
    end
  end

  context 'usando o período dia' do 
    it 'filtra pelo dia atual e dia anterior' do
      filter = Statistics::Filter.build({
        'period' => 'day'
      })

      where = {
        :current => {
          :data => Date.today
        },
        :previous => {
          :data => Date.today - 1.day
        }
      }

      expect(filter).to eq(where)
    end
  end

  context 'usando o período da semana' do 
    it 'filtra pela semana atual e semana anterior' do
      filter = Statistics::Filter.build({
        'period' => 'week'
      })

      where = {
        :current => {
          :data => {
            :'$gte' => Date.today.beginning_of_week,
            :'$lte' => Date.today.end_of_week
          }
        },
        :previous => {
          :data => {
            :'$gte' => Date.today.beginning_of_week - 1.week,
            :'$lte' => Date.today.end_of_week -  1.week
          }
        }
      }

      expect(filter).to eq(where)
    end
  end

  context 'usando o período do més' do
    it 'filtra pelo o mês atual e mês anterior' do
      filter = Statistics::Filter.build({
        'period' => 'month'
      })

      where = {
        :current => {
          :data => {
            :'$gte' => Date.today.beginning_of_month,
            :'$lte' => Date.today.end_of_month
          }
        },
        :previous => {
          :data => {
            :'$gte' => Date.today.beginning_of_month - 1.month,
            :'$lte' => Date.today.end_of_month -  1.month
          }
        }
      }

      expect(filter).to eq(where)
    end
  end

  context 'usando o período por data' do
    it 'filtra pela a data informata e o período anterior' do
      filter = Statistics::Filter.build({
        'period' => 'custom',
        'of' => '15/10/2015',
        'to' => '30/10/2015',
      })

      where = {
        :current => {
          :data => {
            :'$gte' => Date.new(2015, 10, 15),
            :'$lte' => Date.new(2015, 10, 30),
          }
        },
        :previous => {
          :data => {
            :'$gte' => Date.new(2015, 9, 29),
            :'$lte' => Date.new(2015, 10, 14)
          }
        }
      }

      expect(filter).to eq(where)
    end
  end
end

require 'test_helper'

class Integration::AppsControllerTest < ActionController::TestCase
  setup do
    @integration_app = integration_apps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:integration_apps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create integration_app" do
    assert_difference('Integration::App.count') do
      post :create, integration_app: { capture_interval: @integration_app.capture_interval, capture_path: @integration_app.capture_path, domain: @integration_app.domain, name: @integration_app.name, test_path: @integration_app.test_path, update_path: @integration_app.update_path }
    end

    assert_redirected_to integration_app_path(assigns(:integration_app))
  end

  test "should show integration_app" do
    get :show, id: @integration_app
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @integration_app
    assert_response :success
  end

  test "should update integration_app" do
    patch :update, id: @integration_app, integration_app: { capture_interval: @integration_app.capture_interval, capture_path: @integration_app.capture_path, domain: @integration_app.domain, name: @integration_app.name, test_path: @integration_app.test_path, update_path: @integration_app.update_path }
    assert_redirected_to integration_app_path(assigns(:integration_app))
  end

  test "should destroy integration_app" do
    assert_difference('Integration::App.count', -1) do
      delete :destroy, id: @integration_app
    end

    assert_redirected_to integration_apps_path
  end
end

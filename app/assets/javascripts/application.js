// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require color-admin/plugins/jquery/jquery-migrate-1.1.0.min.js
//= require color-admin/plugins/pace/pace.min
//= require color-admin/plugins/jquery-ui/ui/minified/jquery-ui.min
//= require color-admin/plugins/bootstrap/js/bootstrap.min
//= require color-admin/plugins/slimscroll/jquery.slimscroll.min
//= require color-admin/plugins/jquery-cookie/jquery.cookie
//= require datatables/media/js/jquery.dataTables
//= require color-admin/js/apps.min
//= require angular
//= require angular-animate/angular-animate
//= require angular-datatables/dist/angular-datatables
//= require angular-datatables/dist/plugins/fixedheader/angular-datatables.fixedheader
//= require angular-toastr/dist/angular-toastr.tpls
//= require blockui/jquery.blockUI
//= require bootstrap-datepicker/dist/js/bootstrap-datepicker
//= require bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min
//= require angular-paging/dist/paging
//= require_tree .
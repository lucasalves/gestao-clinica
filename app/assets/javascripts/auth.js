// (function($){
//   var Auth = function(selector){
//     this.html = $(selector);

//     this.error = {type: 'user-or-password'}
//     this.url   = this.html.find('form').attr('action');

//     this.addEventListener();
//   };

//   Auth.prototype.addEventListener = function() {
//     this.html.find('form').on('submit', $.proxy(this, 'submit'));
//     this.html.find('input').on('focus', $.proxy(this, 'hideError'));
//   };

//   Auth.prototype.submit = function(e) {
//     e.preventDefault();
//     this.login();
//   }
//   Auth.prototype.login = function(){
//     var data = this.getData();     
//     if(!this.valid(data)){
//       return this.displayError();
//     }

//     this.request(data, this.response);
//   };

//   Auth.prototype.getData = function(){
//     return {
//       email_or_login: this.html.find('#email-or-login').val(),
//       password: this.html.find('#password').val()
//     };
//   };

//   Auth.prototype.valid = function(data){
//     if(
//       !data.email_or_login.length ||
//       !data.password.length
//     ){
//       this.error.type = 'empty-field';
//       return false
//     }

//     return true;
//   };

//   Auth.prototype.displayError = function(){
//     this.html.find('.' + this.error.type).fadeIn();
//   };

//   Auth.prototype.hideError = function(){
//     this.html.find('.error').fadeOut();
//   };

//   Auth.prototype.request = function(data, addEventListener) {
//     $.post(this.url, data, addEventListener, 'json');
//   };

//   Auth.prototype.response = function(response) {
//     if(!response.authentication_token){
//       this.error.type = 'user-or-password';
//       this.displayError();
//       return;
//     }

//     location.href = '/';
//   };

//   $(function(){
//     window.auth = new Auth('.login');
//   });
  
// }(jQuery));
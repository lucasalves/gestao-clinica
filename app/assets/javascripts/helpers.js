(function(App){

  App.UI = {
    _options: {
      message: '<div class=" fade in"><span class="spinner"></span></div>',
      css: {
        border: 'none', 
        padding: '5px', 
        'background-color': 'transparent', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: 0.6, 
        color: '#fff',
        cursor: 'wait'
      }
    },
    block  : function(element){
      if(!element){
        $.blockUI(this._options);
      }else{
        $(element).block(this._options);
      }
    },
    unblock : function(element){
      if(!element){
        $.unblockUI();
      }else{
        $(element).unblock();
      }
    }
  };

}(App));

// $(function(){
//   $(window).scroll(function(){
//     var element = $('.fixed-scroll'),
//         scroll  = $(this).scrollTop() + $('.header').height();

//     if(scroll >= element.offset().top){
//       element.animate({top: $(this).scrollTop() });
//     }else{
//       // element.animate({top: $(this).scrollTop());
//     }
//   });
// });
app.controller('StatisticsMedicos', ['$scope', '$controller', '$timeout', function($scope, $controller, $timeout){
  angular.extend($scope, $controller('Statistics', {
    $scope: $scope
  }));

  /**
   * @name URL
   * @description URL para obter as estatisticas
   * @type String
   */
  $scope.url = '/atendimentos/statistics/medicos.json';

  /**
   *
   * @name Númetos
   * @description Numeros de estatisticas dos atendimentos
   * @type Array
   * 
   */
  $scope.medicos = [];
  $scope.dados   = [];

  /**
   *
   * @name Seta Dados
   * @description Seta os dados da estatisticas
   * @param Object
   * @return void
   *
   */
  $scope.setData = function(data){
      $scope.medicos = data;
      $scope.dados   = data;

      console.log('before apply!');
  };
}]);
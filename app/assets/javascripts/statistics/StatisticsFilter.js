app.controller('StatisticsFilter', ['$scope', '$element', function($scope, $element){

  /**
   * 
   * @name Filtro
   * @description Filtro para consulta
   * @type {Object}
   * 
   */
  $scope.filter = {
    period: 'all'
  };

  /**
   * 
   * @name Seta o periodo
   * @description Atribui o perido no filtro para consulta e faz a consulta
   * @param {String}
   * @param {String}
   * @param {String}
   * @return void
   * 
   */
  $scope.setFilter = function(period, of, to){
    $scope.selected(period);

    $scope.filter = {
      period: period
    };

    if(period === 'custom'){
      $scope.filter.of = of;
      $scope.filter.to = to;
    }else{
      $scope.filter.of = '';
      $scope.filter.to = '';

      $scope.hideDateRange();
    }

    $scope.$broadcast('statistics - reload', $scope.getFilter());
  };

  /**
   *
   * @name Selecionado
   * @description Manipula a interface para conresponder com o filtro atual
   * @param {String} periodo selecionado
   * @return void
   * 
   */
  $scope.selected = function(period){
    $element.find('button').removeClass('active');
    $element.find('.' + period).addClass('active');
  };

  /**
   * 
   * @name Exibe data periodo
   * @description Exibe o filtro de datas
   * @return void
   * 
   */
  $scope.displayDateRange = function(){
    $scope.selected('custom');
    $element.find('.range-filter').fadeIn();
  };

  /**
   * 
   * @name Esconde data periodo
   * @description Exibe o filtro de datas
   * @return void
   * 
   */
  $scope.hideDateRange = function(){
    $element.find('.range-filter').fadeOut();
  };

  /**
   *
   * @name Obtem o Filtro
   * @description retorna o filtro atual
   * @return {Object}
   * 
   */
  $scope.getFilter = function(){
    return $scope.filter;
  };
}]);
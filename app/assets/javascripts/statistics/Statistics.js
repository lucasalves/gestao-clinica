app.controller('Statistics', ['$scope', function($scope){

  /**
   * @name URL
   * @description URL para obter as estatisticas
   * @type {String}
   */
  $scope.url = '';

  /**
   *
   * @name Inicializador
   * @description Responsavel por inicializar as estatisticas
   * @return void
   *
   */
  $scope.init = function(){
    $scope.request();
  };

  /**
   *
   * @name Seta Dados
   * @description Seta os dados da estatisticas
   * @param Object
   * @return void
   *
   */
  $scope.setData = function(data){
    console.log('method required - $scope.setData');
  };

  /**
   *
   * @name Resposta
   * @description Trata a resposta do Ajax
   * @param Object
   * @return void
   *
   */
  $scope.response = function(response){
    App.UI.unblock();

    $scope.setData(response.data);
    $scope.$apply();
  };

  /**
   *
   * @name Requisição
   * @description Faz pedido das estatisticas
   * @return void
   *
   */
  $scope.request = function(){
    App.UI.block();

    $.get(
        $scope.url,
        $scope.getFilter(),
        $scope.response,
        'json'
      )
      .error(function(){
        App.UI.unblock()
      });
  };

  //AddEventListener
  $scope.$on('statistics - reload', $scope.request);
}]);
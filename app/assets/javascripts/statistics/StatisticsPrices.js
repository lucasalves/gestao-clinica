app.controller('StatisticsPrices', ['$scope',  '$controller', function($scope, $controller){
  
  angular.extend($scope, $controller('Statistics', {
    $scope: $scope
  }));

  /**
   * @name URL
   * @description URL para obter as estatisticas
   * @type {String}
   */
  $scope.url = '/atendimentos/statistics/prices.json';

  /**
   *
   * @name Preços
   * @description Preços dos atendimentos
   * @type Object
   * 
   */
  $scope.prices = {
    total  : '',
    growth : 0.0
  };

  /**
   *
   * @name Seta Dados
   * @description Seta os dados da estatisticas
   * @param Object
   * @return void
   *
   */
  $scope.setData = function(data){
    $scope.prices  = data;
  }
}]);
app.controller('StatisticsAtendimentos', ['$scope', '$controller', function($scope, $controller){
  
  angular.extend($scope, $controller('Statistics', {
    $scope: $scope
  }));

  /**
   * @name URL
   * @description URL para obter as estatisticas
   * @type {String}
   */
  $scope.url = '/atendimentos/statistics/count.json';

  /**
   *
   * @name Númetos
   * @description Numeros de estatisticas dos atendimentos
   * @type Object
   * 
   */
  $scope.numbers = {
    total  : '',
    growth : 0.0
  };


  /**
   *
   * @name Seta Dados
   * @description Seta os dados da estatisticas
   * @param Object
   * @return void
   *
   */
  $scope.setData = function(data){
    $scope.numbers = data;
  };
}]);
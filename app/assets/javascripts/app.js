app = angular.module(
  'GestaoClinica',
  [
    'datatables',
    'datatables.fixedheader',
    'ngAnimate',
    'toastr',
    'bw.paging'
  ]
);


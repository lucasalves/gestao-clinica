app.controller('SaudeSuplementarAtendimento', ['$scope', 'DTOptionsBuilder', 'DTColumnBuilder', '$compile', 'toastr', '$element', function($scope, DTOptionsBuilder, DTColumnBuilder, $compile, toastr, $element){
  App.UI.block();
  /**
   *
   * @type String
   * @name Valor Total
   * @description Valor total dos atendimentos selecionados
   *              
   * 
   */
  $scope.total = '0,00';

  /**
   *
   * @type Number
   * @name Máximo de atendimentos
   * @description Contem o número máximo de atendimentos permitidos
   *              para envio
   * 
   */
  $scope.maxAtendimentos = 100;

  /**
   *
   * @type Object
   * @name Opções
   * @description Opções para visualização da tabeda de atendimento
   *
   */
  $scope.options = DTOptionsBuilder.fromSource('/saude-suplementar/atendimentos/list')
                                   .withOption('fnRowCallback', function(nRow) {
                                      $compile(nRow)($scope);
                                      App.UI.unblock();
                                   })
                                   // .withOption('bAutoWidth', false)
                                   .withDataProp('data')
                                   .withDisplayLength(100);

  /**
   * 
   * @name Checkbox
   * @description Cria checkbox para selecionar o atendimento
   * @param Object
   * @param String
   * @param Object
   * @param Object
   * @return String
   *
   */
  $scope.checkbox = function(data, status, current, info){
    var checked = $scope.isChecked(data.atendimento) ? 'checked="checked"' :  '';
    return '<input ' + checked + ' id="atendimento-' + data.atendimento + '" type="checkbox" value="' + data.atendimento + '" ng-click="selectAtendimento('+ data.atendimento +', $event); $event.stopPropagation();"/>';
  };

  /**
   * 
   * @name price
   * @description Preço do atendimento
   * @param Float
   * @param String
   * @param Object
   * @param Object
   * @return String
   *
   */
  $scope.price = function(price, status, current, info){
    return Number(price).toFixed(2).replace('.', ',');
  };

  /**
   *
   * @type Object
   * @name Colunas
   * @description Colunas exibidas na tabela
   *
   */
  $scope.columns = [
    DTColumnBuilder.newColumn(null).withTitle('#').withClass('select-column').renderWith($scope.checkbox),
    DTColumnBuilder.newColumn('atendimento').withClass('text-center').withTitle('Atendimento'),
    DTColumnBuilder.newColumn('nomeContratado').withTitle('Convênio'),
    DTColumnBuilder.newColumn('paciente').withTitle('Paciente'),
    DTColumnBuilder.newColumn('medico').withTitle('Médico'),
    DTColumnBuilder.newColumn('valorTotal').withTitle('Valor').renderWith($scope.price)
  ];

  /**
   *
   * @type Object
   * @name DataTable Instancia
   * @description Contem a instancia da tabela pata manipulação
   *
   */
  $scope.dtInstance = {};

  /**
   *
   * @type Array
   * @name Lote
   * @description Contem o lote de atendimentos
   *              para solicitação de faturamento
   *
   */
   $scope.lot = [];

  /**
   * 
   * @name Recarrega
   * @description Atualiza a tabela
   * @return void
   *
   */
  $scope.reload = function(){
    $scope.lot   = [];
    $scope.total = '0,00';
    $scope.dtInstance.reloadData();
  };

  /**
   *
   * @name Obtem Atendimento
   * @description Obtem o Atendimento pelo ID
   * @param String
   * @return Object
   *
   */
  $scope.getAtendimento = function(id){
    var data = $scope.dtInstance.DataTable.rows().data();

    for (var i = 0; i < data.length; i++) {
      if(data[i].atendimento === id){
        return data[i]
      }
    }
  };

  /**
   *
   * @name É checado
   * @description Verifica se é checado
   * @param String
   * @return Object
   *
   */
  $scope.isChecked = function(id){
    for (var i = 0; i < $scope.lot.length; i++) {
      if($scope.lot[i].atendimento === id){
        return true;
      }
    }

    return false;
  };

  /**
   * 
   * @name Seleciona Atendimentos
   * @description Seleciona os atendimentos e adiciona no lote
   * @param Number
   * @param Object
   * @return void
   *
   */
  $scope.selectAtendimento = function(id, event){
    for (var i = 0; i < $scope.lot.length; i++) {
      if($scope.lot[i].atendimento === id){
        $scope.lot.splice(i, 1);
        $scope.calculateTotal();

        $element.find('table input#atendimento-' + id)
                .prop('checked', false);
        return;
      }
    }

    if($scope.lot.length === $scope.maxAtendimentos){
      event.preventDefault();

      toastr.warning(
        'Só é possivel enviar 100 atendimentos por lote',
        'Número Máximo de Atendimento'
      );
      return;
    }

    var selected = $scope.getAtendimento(id);
    $scope.lot.push(selected);
    $scope.calculateTotal();

    $element.find('table input#atendimento-' + selected.atendimento)
            .prop('checked', true);
  };

  /**
   * 
   * @name Seleciona Todos
   * @description Seleciona o numero máximo de atendimento
   * @return void
   *
   */
  $scope.selectAll = function(event){
    var data = $scope.dtInstance.DataTable.rows().data(),
        max  = (
          $scope.maxAtendimentos > data.length ? 
            data.length - 1 : 
            $scope.maxAtendimentos
        ),
        i = 0;

    $scope.lot = [];
    $element.find('table input').removeAttr('checked');


    if(!$element.find(event.target).is(':checked')){
      return;
    }

    for(i; i < max; i++) {
      $scope.selectAtendimento(data[i].atendimento, event);
    };
  };

  /**
   * 
   * @name Calcula Valor Total
   * @desciption Calcula o valor total de lotes selecionados
   * @return void
   *
   */
  $scope.calculateTotal = function(){
    var total = 0;

    angular.forEach($scope.lot, function(atendimento){
      total += atendimento.valorTotal;
    });

    total = total.toFixed(2).replace(/./g, function(c, i, a) {
      return i && c !== "," && ((a.length - i) % 3 === 0) ? '.' + c : c;
    }).replace(/\.\./g, ',');

    $scope.total = total;
  };

  /**
   * 
   * @name Obtem Convenio
   * @description Obtem o convenio selecionado
   * @return String
   *
   */
  $scope.getConvenio = function(){
    for (var i = 0; i < $scope.lot.length; i++) {
      if($scope.lot[i].nomeContratado !== ''){
        return $scope.lot[i].nomeContratado;
      }
    }

    return null;
  };

  /**
   * 
   * @name Obtem Atendimentos
   * @description Obtem Atendimentos
   * @return String
   *
   */
  $scope.getAtendimentos = function(){
    var atendimentos = [];

    for (var i = 0; i < $scope.lot.length; i++) {
      atendimentos.push($scope.lot[i]._id['$oid']);
    }

    return atendimentos;
  };

  /**
   *
   * @name Criar
   * @description Cria o lotes com os atendimentos
   * @return void
   *
   */
  $scope.create = function(){
    App.UI.block();
    $scope.request({
      convenio     : $scope.getConvenio(),
      atendimentos : $scope.getAtendimentos()
    }, $scope.response);
  };

  /**
   *
   * @name Requisição
   * @description Faz requisição para criação de lote
   * @return void
   *
   */
  $scope.request = function(data, addEventListener){
    $.post('/saude-suplementar/lotes', data, addEventListener)
     .done(function(){
       App.UI.unblock();
     });
  };

  /**
   *
   * @name Resposta
   * @description Tra a resposta do pedido de criação de lote
   * @return void
   *
   */
  $scope.response = function(data){
    toastr.success('Lote adicionado na fila de faturamento!');
    $scope.reload();
    
    $scope.$apply();
  }

}]);
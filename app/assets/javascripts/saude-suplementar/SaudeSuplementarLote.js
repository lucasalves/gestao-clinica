app.controller('SaudeSuplementarLote', ['$scope', '$element', 'toastr', function($scope, $element, toastr){
  
  /**
   *
   * @name URL
   * @description URL de requisição
   * @type {String}
   * 
   */
  $scope.url = '/faturamento/saude_suplementar/atendimentos';

  /**
   *
   * @name Atendimentos
   * @description Lista de atendimentos
   * @type {Array}
   * 
   */
  $scope.atendimentos = [];

  /**
   *
   * @name Selecionados
   * @description Lista de atendimentos selecionados para gerar o lote
   * @type {Array}
   * 
   */
  $scope.selecteds = [];

  /**
   *
   * @name Página atual
   * @description Marcador de página atual
   * @type {Number}
   * 
   */
  $scope.currentPage = 1;
  
  /**
   *
   * @name Tamanho da Página
   * @description Quantidade de itens por página
   * @type {Number}
   * 
   */
  $scope.pageSize = 50;

  /**
   *
   * @name Total
   * @description Quantidade total de itens
   * @type {Number}
   * 
   */
  $scope.total = 0;

  /**
   *
   * @name Preço
   * @description Preço total dos atendimentos selecionados
   * @type {String}
   * 
   */
  $scope.price = '';

  /**
   *
   * @name Ação
   * @description Recebe a ação de alteração de página
   * @return void
   * 
   */
  $scope.action = function(page){
    $scope.request(page);
  };

  /**
   *
   * @name Requisição
   * @description Requisição
   * @param Number 
   * @return void
   * 
   */
  $scope.request = function(page){
    App.UI.block();

    page = page || 1

    $.get($scope.url, {page: page}, $scope.response, 'json');
  };

  /**
   *
   * @name Resposta
   * @description Resposta da requisição
   * @param Object
   * @return void
   * 
   */
  $scope.response = function(data){
    App.UI.unblock();

    $scope.total    = data.total;

    $scope.atendimentos = data.data;

    $scope.$apply();
  };

  /**
   *
   * @name Está selecionado ?
   * @description Verifica se atendimento esta na lista dos selecionados
   * @param {Obeject} atendimento objeto de atendimento
   * @return {boolean}
   * 
   */
  $scope.isSelected = function(atendimento){
    var i = 0;

    for (i; i < $scope.selecteds.length; i++) {
      if($scope.selecteds[i].id === atendimento.id){
        return true;
      }
    }

    return false;
  };


  /**
   *
   * @name Seleciona
   * @description Seleciona o atendimento para o lote
   * @param {Obeject} atendimento objeto de atendimento
   * @return {boolean}
   * 
   */
  $scope.select = function(atendimento){
    var i = 0;

    for (i; i < $scope.selecteds.length; i++) {
      if($scope.selecteds[i].id === atendimento.id){
        $scope.selecteds.splice(i, 1);
        $scope.sum();

        return false;
      }
    }

    if($scope.selecteds.length === 100){
      toastr.warning(
        'Só é possivel enviar 100 atendimentos por lote',
        'Número Máximo de Atendimento'
      );
    }

    $scope.selecteds.push(atendimento);
    $scope.sum();

    return true;
  };


  /**
   *
   * @name  Soma
   * @description Faz a soma dos atendimentos selecionados
   * @return void
   * 
   */
  $scope.sum = function(){
    var total = 0,
        i     = 0;
    
    for (i; i < $scope.selecteds.length; i++) {
        total += $scope.selecteds[i].valor;
    }

    $scope.price = total.money(2, ',', '.');
  };
}]);

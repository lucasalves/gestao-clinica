//Set Default Language
$.fn.datepicker.defaults.language = 'pt-BR';

$(function(){
  //Auto call for range inputs
  $('.input-daterange').datepicker("clearDates");
});
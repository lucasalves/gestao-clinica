json.array!(@integration_apps) do |integration_app|
  json.extract! integration_app, :id, :name, :domain, :test_path, :capture_path, :update_path, :capture_interval
  json.url integration_app_url(integration_app, format: :json)
end

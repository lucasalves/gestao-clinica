json.total @count

json.data do
  json.array! @atendimentos do |atendimento|
    json.id   atendimento.id
    json.data l(atendimento.data)
    
    json.paciente do 
      json.nome atendimento.paciente[:nome]
    end

    json.profissional do 
      json.nome atendimento.profissional[:nome]
    end

    json.valor atendimento.valor_total
    json.valor_total number_to_currency(atendimento.valor_total)
  end
end



class Statistics::Tool
  
    # Calcula o crecimento em porcentagem entre dois periodos
    # 
    # @param [ Integer ] periodo atual
    # @param [ Integer ] periodo anterior
    # 
    # @return [ Float ]
    def self.growth(current, previous)
      return 0 unless previous != 0

      percentage = (Float(current - previous) / previous) * 100
      percentage.round(2)
    end

    # Formata valores monetários
    # 
    #
    # @param [ Float ] valor
    # 
    # @return [ String ] valor monetário
    def self.format_price(value)
      ActionController::Base.helpers.number_to_currency(
          value,
          unit: "R$", 
          separator: ",",
          delimiter: "."
        )
    end

end
class Statistics::Filter
  
  # Constroi o filtro
  #
  # @param [ Hash ] filtro aplicado
  # 
  # @return [ Hash ]
  def self.build(filter)
    {
      :current  => current(filter),
      :previous => previous(filter),
    }
  end

  # Monta o filtro do período atual
  # 
  # @param [ Hash ] filtro aplicado
  # 
  # @return [ Hash ]
  def self.current(filter)
    period = filter['period']
    
    return self.send(period, filter) if period == 'custom'
    self.send(period)
  end

  # Monta o filtro do período anterior
  # 
  # @param [ Hash ] filtro aplicado
  # 
  # @return [ Hash ]
  def self.previous(filter)
    period = filter['period']

    if period == 'all'
      return {}
    end

    base = current(filter)

    if period == 'custom'
      decrement = (base[:data][:'$gte']..base[:data][:'$lte']).count
      decrement = (decrement).days
    else
      decrement = 1.send(period)
    end
    

    if period == 'day'
      base[:data] = base[:data] - decrement
    else
      base[:data] = {
        '$gte': base[:data][:'$gte'] - decrement,
        '$lte': base[:data][:'$lte'] - decrement
      }
    end

    base
  end

  # Obtem o filtro do dia
  #
  # @return [ Hash ]
  def self.day()
    {
      :data => Date.today
    }
  end

  # Obtem o filtro da semana
  #
  # @return [ Hash ]
  def self.week()
    {
      :data => {
        '$gte': Date.today.beginning_of_week,
        '$lte': Date.today.end_of_week
      }
    }
  end

  # Obtem o filtro do mês
  #
  # @return [ Hash ]
  def self.month()
    {
      :data => {
        '$gte': Date.today.beginning_of_month,
        '$lte': Date.today.end_of_month
      }
    }
  end

  # Obtem o filtro de todo o período
  #
  # @return [ Hash ]
  def self.all()
    {}
  end

  # Obtem o filtro pela data informada
  #
  # @return [ Hash ]
  def self.custom(date)
    {
      :data => {
        '$gte': date['of'].to_date,
        '$lte': date['to'].to_date
      }
    }
  end
end
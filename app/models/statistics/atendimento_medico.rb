class Statistics::AtendimentoMedico

  # Seta o filtro atual para estatisticas
  #
  # @param [ Hash ] filtro aplicado
  # 
  # @return [ Hash ]
  def initialize(filter)
    @filter  = filter
    @medicos = load
  end

  # Obtem as estátisticas de médicos
  # 
  # @return [ Array ]
  def medicos
    current  = aggregation_medicos(@filter[:current])
    previous = aggregation_medicos(@filter[:previous])

    format(current, previous)
  end

  # Faz a agregação das estátisticas dos médicos com base
  # 
  # @param [ Hash ] condição para obter as estátisticas
  # 
  # @return [ Array ]
  def aggregation_medicos(where)
    medicos = Integration::Atendimento.collection.aggregate([
      {
        '$match' => where
      },
      {
        '$group' => {
          '_id' => '$profissional.crm',
          'price' => {
            '$sum' => '$valor_total'
          },
          'atendimento' => {
            '$sum' => 1
          }
        }
      },
      {
        '$sort' => {'total' => -1}
      }
    ])

    medicos.to_a
  end

  private
    # Carrega todos os médicos
    # 
    # @return [ Array ] médicos
    def load
      medicos = Integration::Atendimento.collection.aggregate([
        {
          '$group' => {
            '_id'                     => '$profissional.crm',
            'crm'                     => { '$first' => '$profissional.crm' },
            'nome'                    => { '$first' => '$profissional.nome' },
            'cbos'                    => { '$first' => '$profissional.cbos' },
            'percentual_visualizacao' => { '$first' => '$profissional.percentual_visualizacao' },
            'codigo_conselho'         => { '$first' => '$profissional.codigo_conselho' },
            'uf_conselho'             => { '$first' => '$profissional.uf_conselho' },
            'tipo'                    => { '$first' => '$profissional.tipo' }
          }
        }
      ])

      medicos
    end

    # Formata as estatisticas dos médicos
    # 
    # @param [ Array ] estátisticas atuais
    # @param [ Array ] estátisticas anteriores
    #
    # @return [ Array ] estátisticas formtadas
    def format(current, previous)
      statistics = []

      @medicos.each do |medico|
        statistic = find_statistics(medico[:crm], current, previous)

        medico[:statistics] = {
          :atendimentos => {
            :total  => statistic[:current][:atendimento],
            :growth => Statistics::Tool.growth(
              statistic[:current][:atendimento],
              statistic[:previous][:atendimento]
            )
          },
          :prices => {
            :total  => Statistics::Tool.format_price(statistic[:current][:price]),
            :value  => statistic[:current][:price],
            :growth => Statistics::Tool.growth(
              statistic[:current][:price],
              statistic[:previous][:price]
            )
          }          
        }
        
        statistics << medico
      end

      statistics.sort_by { |statistic| statistic[:statistics][:prices][:value] }.reverse!
    end

    # Procura a estátiscas do médico atual e anterior
    #
    # @param [ String ] CRM do médico
    # @param [ Array ]  estátisticas atuais
    # @param [ Array ]  estátisticas anteriores
    # 
    # @return [ Hash ]
    def find_statistics(crm, current, previous)
      current  = find(crm, current)
      previous = find(crm, previous)

      {
        :current  => current,
        :previous => previous 
      }
    end

    # Procura em um médico em um array
    #
    # @param [ String ] CRM
    # @param [ Array ]  estátisticas
    # 
    # @return [ Hash ]
    def find(crm, statistics)
      found = statistics.find_index do |statistic|
        statistic[:_id] == crm
      end

      if found.nil?
        found = {
          :price       => 0,
          :atendimento => 0
        }
      else
        found = statistics[found]
      end

      found
    end
end
class Statistics::Atendimento
  
  # Seta o filtro atual para estatisticas
  #
  # @param [ Hash ] filtro aplicado
  # 
  # @return [ Hash ]
  def initialize  (filter = nil)
    @filter = Statistics::Filter.build(filter)
  end

  # Faz a contagem de atendimentos
  #
  # @return [ Hash ] estatisticas de totais
  #
  def count
    current  = Integration::Atendimento.where(@filter[:current]).count
    previous = Integration::Atendimento.where(@filter[:previous]).count

    statistics = {
      :total  => current,
      :growth => Statistics::Tool.growth(current, previous)
    }
  end

  # Faz a contagem de atendimentos
  #
  # @return [ Hash ] estatisticas de totais
  #
  def prices
    current  = Integration::Atendimento.where(@filter[:current]).sum(:valor_total)
    previous = Integration::Atendimento.where(@filter[:previous]).sum(:valor_total)
    
    statistics = {
      :total  => Statistics::Tool.format_price(current),
      :growth => Statistics::Tool.growth(current, previous)
    }
  end

  # Obtem as estátisticas de médicos
  # 
  # @return [ Array ]
  def medicos
    statistic = Statistics::AtendimentoMedico.new(@filter)
    statistic.medicos
  end

  def aggregation_medicos(where)
    medicos = Integration::Atendimento.collection.aggregate([
      {
        '$match' => where
      },
      {
        '$group' => {
          '_id' => '$profissional.crm',
          'total' => {
            '$sum' => '$valor_total'
          },
          'atendimento' => {
            '$sum' => 1
          }
        }
      },
      {
        '$sort' => {'total' => -1}
      }
    ])

    medicos
  end
end
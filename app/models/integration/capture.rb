class Integration::Capture
  
  def self.process(app)
    app.lock

    log = Integration::Log.new
    log.app = app

    log.started_at = DateTime.now

    begin
      data = request(app)

      counter = save(data['data'], app)

      log.updated_count   = counter[:updated]
      log.created_count   = counter[:created]
      log.no_action_count = counter[:no_action]

      log.checksum      = checksum(data)
    rescue Exception => e
      log.message        = e.message
      log.checksum       = checksum(e.message)

      log.status = false
    end

    log.finalized_at = DateTime.now
    log.save

    app.unlock
  end

  # Faz a requisição dos, atendimentos
  #
  # @param [ Integration::App ] Aplicativo atual
  #
  # @return [ Hash ] Atendimentos
  def self.request(app)
    response = RestClient.get(app.url_capture)
    response = decode(response.body)
  end

  private 
    # Persiste os atendimentos capturados
    #
    # @param [ Hash ] Atendimentos
    # @param [ Integration::App ] Aplicativo atual
    #
    # @return [ Hash ] Contadores das ações tomadas
    def self.save(atendimentos, app)
      count = {
        :no_action => 0,
        :created   => 0,
        :updated   => 0
      }

      atendimentos.each do |atendimento|
        atendimento['app_id'] = app.id
        type = create_or_update(atendimento)

        count[type] += 1
      end

      count
    end

    # Cria ou atualiza o atendimento
    # Baseando no checksum
    #
    # @param [ Hash ] Atendimento
    # 
    # @return [ Symbol ] Ação tomada
    def self.create_or_update(atendimento)
      existing = Integration::Atendimento.where(id: atendimento['id'])
      return create(atendimento) if !existing.exists?

      if existing.first.checksum_changed?(atendimento['checksum'])
        return update(atendimento)
      end

      :no_action
    end

    # Cria um novo atendimento na base de dados
    #
    #
    # @param [ Hash ] Atendimento
    #
    # @return [ Symbol ] Marcação para identificar a chamada do método create
    def self.create(atendimento)
      atendimento['convenio'] = Integration::Atendimento.convenio(atendimento)
      Integration::Atendimento.create(atendimento)

      :created
    end

    # Atualiza o atendimento
    # removendo o item desatualizado e criando o novo
    #
    # @param [ Hash ] Atendimento
    #
    # @return [ Symbol ] Marcação para identificar a chamada do método update
    def self.update(atendimento)
      Integration::Atendimento.where(id: atendimento['id']).delete
      create(atendimento)

      :updated
    end

    def self.decode(data)
      JSON.parse(data)
    end

    def self.checksum(base)
      base = Marshal.dump(base)
      Digest::MD5.hexdigest(base)
    end
end

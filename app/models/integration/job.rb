class Integration::Job < ActiveRecord::Base
  
  # Executa o JOB para captura dos atendimentos
  #
  # @return [ Nil ]
  def self.start
    apps = Integration::App.where(:locked => false)

    apps.each do |app|
      if allowed(app)
        Integration::Capture.process(app)
      end
    end
  end

  private
    # Verifica se o aplicativo pode executar a captura
    #
    # @param [ Integration::App ] Aplicativo
    #
    # @return [ Boolean ]
    def self.allowed(app)
      last = app.logs.last
      return true if last.nil?

      exec_in = last.finalized_at + app.capture_interval.minutes
      exec_in <= DateTime.now
    end
end

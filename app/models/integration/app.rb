class Integration::App < ActiveRecord::Base
  has_many :logs, class_name: 'Integration::Log'

  def url_test
    "#{self.domain}/#{self.test_path}"
  end

  def url_capture
    "#{self.domain}/#{self.capture_path}"
  end

  # Bloqueai o aplicativo para novas buscas
  #
  # @return [ Boolean ]
  def lock
    self.locked = true
    self.save
  end

  # Desbloqueia o aplicativo para novas buscas
  #
  # @return [ Boolean ]
  def unlock
    self.locked = false
    self.save
  end
end

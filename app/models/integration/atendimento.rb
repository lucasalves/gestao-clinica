class Integration::Atendimento
  include Mongoid::Document
  include Mongoid::Timestamps

  field :id,            :type => Integer
  field :app_id,        :type => Integer
  field :data,          :type => Date
  field :autorizacao,   :type => Hash
  field :paciente,      :type => Hash
  field :convenio,      :type => Hash
  field :plano,         :type => Hash
  field :profissional,  :type => Hash
  field :procedimentos, :type => Array
  field :valor_total,   :type => Float
  field :checksum,      :type => String

  field :billed,        :type => Boolean, default: false

  belongs_to :lote, :class_name => 'Integration::Lote'

  # Verifica se o atendimento foi alterado
  # Baseando no checksum
  #
  # @param [ String ] Checksum (novo)
  # 
  # @return [ Boolean ]
  def checksum_changed?(new_checksum)
    self.checksum != new_checksum
  end

  # Obtem os dados do convenio
  #
  # @param [ Hash ] Atendimento
  #
  # @return [ Hash ] Convenio
  def self.convenio(atendimento)
    convenio = Covenant.where(:name => atendimento['plano']['nome']).first
    return {} if convenio.nil?

    {
      :id   => convenio.id,
      :nome => convenio.name,
    }
  end
  
end

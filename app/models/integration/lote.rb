class Integration::Lote
  include Mongoid::Document
  include Mongoid::Timestamps

  field :user_id, :type => Integer
  field :numero,  :type => Integer
  field :status,  :type => Boolean, default: true
  field :billed,  :type => Boolean, default: false

  field :xml,     :type => String
  field :pdf,     :type => String

  has_many :atendimentos, :class_name => 'Integration::Atendimento'

  # Faz o calculo do preço dos atendimentos
  #
  # @return [ String ]
  def valor
    self.atendimentos.sum(:valor_total)
  end
end

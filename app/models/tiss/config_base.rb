class Tiss::ConfigBase

  def initialize(covenant, guide)
    @covenant = covenant
    @guide    = guide
  end

  def company
    Company.last
  end

  def registry()
    registry = RegistrationCovenant.joins(:covenant).where('covenants.name' => @covenant).last
  end

  def enterprise_code()
    registry = registry()
    return registry.code if !registry.code.nil?

    registry.company.cnpj.gsub /[\/|\.|\-]/, ''
  end

  def enterprise_cnpj()
    company.cnpj.gsub /[\/|\.|\-]/, ''
  end

  def enterprise_cnes()
    company.cnes
  end

  def enterprise_name()
    registry = registry()
    return registry.name if !registry.name.nil?

    registry.company.name
  end

  def sequential()
    random = Random.new
    random.rand(10000..100000)
  end
end

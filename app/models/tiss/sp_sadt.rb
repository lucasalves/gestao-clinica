class Tiss::SpSadt

  def self.invoice
    Tiss::Guia::SpSadt.new('GEAP')

    data = [{
      :cabecalho => {
        :registro => 323080,
        :numero_guia_prestador   => '0534893821'
      },
      :autorizacao => {
        :numero => '0534893821',
        :data   => '2014-08-14'
      },
      :beneficiario => {
        :carteira => '02020066002200005',
        :atendimento_rn => 'N',
        :nome => 'ALINE OLIVEIRA RABELO DE SOUZA',
        :cns  => '898001429527333'
      },
      :solicitante => {
        :contratado => {
          :cnpj => 61699567000192,
          :nome => 'ASSOCIAÇÃO PAULISTA P/ DESENV. DA MEDICINA - HOSPITAL SÃO PAULO'
        },
        :profissional => {
          :nome     => 'ANDREA FERNANDES DE OLIVEIRA',
          :conselho => 6,
          :numero   => 99442,
          :uf       => 35,
          :cbos     => 225235,
        }
      },
      :solicitacao => {
        :data                => '2014-08-14',
        :carater_atendimento => 1
      },
      :executante => {
        :contratado => {
          :codigo_prestador_na_operadora => '61699567000192',
          :nome => 'ASSOCIAÇÃO PAULISTA P/ DESENV. DA MEDICINA - HOSPITAL SÃO PAULO'
        },
        :cnes => 2077485
      },
      :atendimento => {
        :tipo                => '04',
        :indicacao_acidente  => 2,
        # :tipo_consulta       => 1,
        :motivo_encerramento => 41
      },
      :procedimentos => [
        {
          :data         => '2014-08-14',
          :procedimento => {
            :codigo_tabela => 22,
            :codigo        => 10101012,
            :descricao     => 'Em Consultorio (No Horario Normal Ou Preestabelecido)',
          },
          :quantidade_executada => 3,
          :reducao_acrescimo    => 0,
          :valor_unitario       => 54.00,
          # :valor_total          => 54.00,
          :equipe => {
            :grau_part => 12,
            :cod_profissional => {
              :cpf => 67337783434
            },
            :nome_profissional => 'ANDREA FERNANDES DE OLIVEIRA',
            :conselho          => 6,
            :numero_conselho_profissional => 99442,
            :uf   => 35,
            :cbos => 225235
          }
        }
      ],
      # :outras_despesas => {},
      # :valor_total => {
      #   :valor_procedimentos => 54.00,
      #   :valor_total_geral   => 54.00
      # }
    },
    {
      :cabecalho => {
        :registro => 323080,
        :numero_guia_prestador   => '0534893821'
      },
      :autorizacao => {
        :numero => '0534893821',
        :data   => '2014-08-14'
      },
      :beneficiario => {
        :carteira => '02020066002200005',
        :atendimento_rn => 'N',
        :nome => 'ALINE OLIVEIRA RABELO DE SOUZA',
        :cns  => '898001429527333'
      },
      :solicitante => {
        :contratado => {
          :cnpj => 61699567000192,
          :nome => 'ASSOCIAÇÃO PAULISTA P/ DESENV. DA MEDICINA - HOSPITAL SÃO PAULO'
        },
        :profissional => {
          :nome     => 'ANDREA FERNANDES DE OLIVEIRA',
          :conselho => 6,
          :numero   => 99442,
          :uf       => 35,
          :cbos     => 225235,
        }
      },
      :solicitacao => {
        :data                => '2014-08-14',
        :carater_atendimento => 1
      },
      :executante => {
        :contratado => {
          :codigo_prestador_na_operadora => '61699567000192',
          :nome => 'ASSOCIAÇÃO PAULISTA P/ DESENV. DA MEDICINA - HOSPITAL SÃO PAULO'
        },
        :cnes => 2077485
      },
      :atendimento => {
        :tipo                => '04',
        :indicacao_acidente  => 2,
        # :tipo_consulta       => 1,
        :motivo_encerramento => 41
      },
      :procedimentos => [
        {
          :data         => '2014-08-14',
          :procedimento => {
            :codigo_tabela => 22,
            :codigo        => 10101012,
            :descricao     => 'Em Consultorio (No Horario Normal Ou Preestabelecido)',
          },
          :quantidade_executada => 10,
          :reducao_acrescimo    => 0,
          :valor_unitario       => 54.00,
          # :valor_total          => 54.00,
          :equipe => {
            :grau_part => 12,
            :cod_profissional => {
              :cpf => 67337783434
            },
            :nome_profissional => 'ANDREA FERNANDES DE OLIVEIRA',
            :conselho          => 6,
            :numero_conselho_profissional => 99442,
            :uf   => 35,
            :cbos => 225235
          }
        }
      ],
      # :outras_despesas => {},
      # :valor_total => {
      #   :valor_procedimentos => 54.00,
      #   :valor_total_geral   => 54.00
      # }
    }
    ]

    lote = Tiss::Guia::SpSadt.new('GEAP')
    lote.set(data)

    lote
  end

  def self.send(lote)
    # client = Savon.client(
      # wsdl: 'http://auth.careplus.com.br/Tiss3/tissLoteGuiasV3_02_01.svc?wsdl=wsdl0',

      # follow_redirects: true,
      # use_wsa_headers: true
    #   # : 'https://ws.geap.com.br/tiss/tissLoteGuias.asmx?op=tissLoteGuias_Operation'
    #   # :ssl_verify_mode => :none,
    #   # log: true,
      # endpoint: 'http://auth.careplus.com.br/Tiss3/tissLoteGuiasV3_02_00.svc?wsdl',
    #   # endpoint:  "https://ws.geap.com.br/tiss/tissLoteGuias.asmx",
      # namespace: "http://www.ans.gov.br/padroes/tiss/schemas/tissLoteGuiasV3_02_00.wsdl"
    # )

    # pp client.operation(:tissLoteGuias_Operation)
    # pp client.service_name
    # pp client.operations

    guide = {
      :cabecalho => lote.guide[:cabecalho],
      :loteGuias => lote.guide[:prestadorParaOperadora][:loteGuias],
      :hash      => lote.guide[:epilogo][:hash]
    }
    # response = client.call(:tiss_lote_guias_operation, message: guide)
    # pp response.body

    require 'soap/wsdlDriver'

    client = SOAP::WSDLDriverFactory.new( 'http://auth.careplus.com.br/Tiss3/tissLoteGuiasV3_02_00.svc?wsdl' ).create_rpc_driver

    # # pp client.methods.sort

    begin
      result = client.tissLoteGuias_Operation(guide)  
      puts result
    rescue Exception => e
      pp e.message
    end
    

    

    # result

    guide
  end

  def self.build(element, data, xml)
  end
end

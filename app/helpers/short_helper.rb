module ShortHelper

  def status(status)
    render :partial => 'layouts/shared/status', :locals => { :status => status }
  end

end

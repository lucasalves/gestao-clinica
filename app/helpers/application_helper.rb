module ApplicationHelper
  def title(page_title, page_description = '')
    content_for(:title){ page_title }
    content_for(:description){ page_description }
  end

  def menu_item_active(path)
    current_page?(path) ? 'active' : ''
  end
end

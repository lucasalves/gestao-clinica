class Faturamento::SaudeSuplementar::LotesController < ApplicationController

  # GET Lista de lotes gerados
  def index
    @lotes = Integration::Lote.order_by(:created_at => -1).page(page)
  end

  # GET cria um novo lote
  def new
    @atendimentos = Integration::Atendimento.where(:lote_id => {'$exists' => false}).page(page)
    @lote = Integration::Lote.new
  end

  # POST cria o lote
  def create
    p params.to_json
  end
end

class Faturamento::SaudeSuplementar::AtendimentosController < ApplicationController

  # GET Lista de atendimentos que não possui lote
  def index
    @atendimentos = Integration::Atendimento.where(:lote_id => {'$exists' => false}).page(page)
    @count        = Integration::Atendimento.where(:lote_id => {'$exists' => false}).count
  end
end

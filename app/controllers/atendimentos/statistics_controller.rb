class Atendimentos::StatisticsController < ApplicationController

  # Obtem as estatisticas de totais de atendimentos
  def count
    statistics = Statistics::Atendimento.new(params)
    
    respond_to do |format|
      format.json { render json:  { :data => statistics.count } }
    end
  end

  # Obtem os valores totais
  def prices
    statistics = Statistics::Atendimento.new(params)
    
    respond_to do |format|
      format.json { render json: { :data => statistics.prices } }
    end
  end

  #Obtem as estatisticas de médicos
  def medicos
    statistics = Statistics::Atendimento.new(params)
    
    respond_to do |format|
      format.json { render json: { :data => statistics.medicos } }
    end
  end
end

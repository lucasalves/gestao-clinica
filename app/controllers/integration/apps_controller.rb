class Integration::AppsController < ApplicationController
  before_action :set_integration_app, only: [:show, :edit, :update, :destroy]

  # GET /integration/apps
  # GET /integration/apps.json
  def index
    @integration_apps = Integration::App.all
  end

  # GET /integration/apps/1
  # GET /integration/apps/1.json
  def show
  end

  # GET /integration/apps/new
  def new
    @integration_app = Integration::App.new
  end

  # GET /integration/apps/1/edit
  def edit
  end

  # POST /integration/apps
  # POST /integration/apps.json
  def create
    @integration_app = Integration::App.new(integration_app_params)

    respond_to do |format|
      if @integration_app.save
        format.html { redirect_to @integration_app, notice: 'App was successfully created.' }
        format.json { render :show, status: :created, location: @integration_app }
      else
        format.html { render :new }
        format.json { render json: @integration_app.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /integration/apps/1
  # PATCH/PUT /integration/apps/1.json
  def update
    respond_to do |format|
      if @integration_app.update(integration_app_params)
        format.html { redirect_to @integration_app, notice: 'App was successfully updated.' }
        format.json { render :show, status: :ok, location: @integration_app }
      else
        format.html { render :edit }
        format.json { render json: @integration_app.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /integration/apps/1
  # DELETE /integration/apps/1.json
  def destroy
    @integration_app.destroy
    respond_to do |format|
      format.html { redirect_to integration_apps_url, notice: 'App was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_integration_app
      @integration_app = Integration::App.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def integration_app_params
      params.require(:integration_app).permit(:name, :domain, :test_path, :capture_path, :update_path, :capture_interval)
    end
end

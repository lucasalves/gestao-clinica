class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!, :set_user

  # Seta o usuário da sessão para attributo para acesso na view
  def set_user
    @current_user = current_user
  end

  # Obtem a página atual
  def page
    params[:page].nil? ? 1 : params[:page]
  end
end

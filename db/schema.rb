# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160112133043) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "cnpj"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "covenants", force: :cascade do |t|
    t.string   "name"
    t.string   "company_name"
    t.string   "cnpj"
    t.string   "registry_ans"
    t.boolean  "situation"
    t.string   "segmentation"
    t.boolean  "status"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "integration_apps", force: :cascade do |t|
    t.string   "name"
    t.string   "domain"
    t.string   "test_path"
    t.string   "capture_path"
    t.string   "update_path"
    t.integer  "capture_interval"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "status",           default: true
    t.boolean  "test_status",      default: true
    t.boolean  "locked",           default: false
  end

  create_table "integration_logs", force: :cascade do |t|
    t.datetime "started_at"
    t.datetime "finalized_at"
    t.integer  "app_id"
    t.string   "checksum"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "status",          default: true
    t.integer  "updated_count",   default: 0
    t.integer  "created_count",   default: 0
    t.integer  "no_action_count", default: 0
    t.string   "message"
  end

  add_index "integration_logs", ["app_id"], name: "index_integration_logs_on_app_id", using: :btree

  create_table "registration_covenants", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "datetime"
    t.boolean  "status"
    t.integer  "covenant_id"
    t.integer  "company_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "registration_covenants", ["company_id"], name: "index_registration_covenants_on_company_id", using: :btree
  add_index "registration_covenants", ["covenant_id"], name: "index_registration_covenants_on_covenant_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "username"
    t.datetime "birth_date"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "integration_logs", "integration_apps", column: "app_id"
  add_foreign_key "registration_covenants", "companies"
  add_foreign_key "registration_covenants", "covenants"
end

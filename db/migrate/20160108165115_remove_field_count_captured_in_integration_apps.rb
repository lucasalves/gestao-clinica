class RemoveFieldCountCapturedInIntegrationApps < ActiveRecord::Migration
  def change
    remove_column :integration_logs, :count_captured
  end
end

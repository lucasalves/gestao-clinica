class CreateIntegrationApps < ActiveRecord::Migration
  def change
    create_table :integration_apps do |t|
      t.string :name
      t.string :domain
      t.string :test_path
      t.string :capture_path
      t.string :update_path
      t.integer :capture_interval

      t.timestamps null: false
    end
  end
end

class AddFieldMessageInIntegrationApps < ActiveRecord::Migration
  def change
    add_column :integration_logs, :message, :string
  end
end

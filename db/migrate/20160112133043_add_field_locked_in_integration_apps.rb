class AddFieldLockedInIntegrationApps < ActiveRecord::Migration
  def change
    add_column :integration_apps, :locked, :boolean, :default => false
  end
end

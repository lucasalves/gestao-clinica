class AddFieldStatusToIntegrationLogs < ActiveRecord::Migration
  def change
    add_column :integration_logs, :status, :boolean, :default => true
  end
end

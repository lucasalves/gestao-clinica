class CreateRegistrationCovenants < ActiveRecord::Migration
  def change
    create_table :registration_covenants do |t|
      t.string :name
      t.string :code
      t.datetime :datetime
      t.boolean    :status
      t.references :covenant, index: true, foreign_key: true
      t.references :company, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

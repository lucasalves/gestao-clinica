class AddFieldsInIntegrationApps < ActiveRecord::Migration
  def change
    add_column :integration_logs, :updated_count, :integer, :default => 0
    add_column :integration_logs, :created_count, :integer, :default => 0
    add_column :integration_logs, :no_action,     :integer, :default => 0
  end
end

class RenameFieldIntegrationAppsToAppInIntegrationLogs < ActiveRecord::Migration
  def change
    rename_column :integration_logs, :integration_app_id, :app_id
  end
end

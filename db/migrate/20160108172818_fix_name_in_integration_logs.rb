class FixNameInIntegrationLogs < ActiveRecord::Migration
  def change
    rename_column :integration_logs, :no_action, :no_action_count
  end
end

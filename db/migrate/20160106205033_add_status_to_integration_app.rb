class AddStatusToIntegrationApp < ActiveRecord::Migration
  def change
    add_column :integration_apps, :status, :boolean, :default => true
  end
end

class CreateCovenants < ActiveRecord::Migration
  def change
    create_table :covenants do |t|
      t.string  :name
      t.string  :company_name
      t.string  :cnpj
      t.string  :registry_ans
      t.boolean :situation
      t.string  :segmentation
      t.boolean :status

      t.timestamps null: false
    end
  end
end

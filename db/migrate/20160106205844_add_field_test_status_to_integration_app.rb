class AddFieldTestStatusToIntegrationApp < ActiveRecord::Migration
  def change
    add_column :integration_apps, :test_status, :boolean, :default => true
  end
end

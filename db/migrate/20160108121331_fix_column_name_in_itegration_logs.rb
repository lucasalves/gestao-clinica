class FixColumnNameInItegrationLogs < ActiveRecord::Migration
  def change
    rename_column :integration_logs, :hash, :checksum
  end
end

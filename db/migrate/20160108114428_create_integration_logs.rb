class CreateIntegrationLogs < ActiveRecord::Migration
  def change
    create_table :integration_logs do |t|
      t.datetime :started_at
      t.datetime :finalized_at
      t.references :integration_app, index: true, foreign_key: true
      t.integer :count_captured
      t.string :hash

      t.timestamps null: false
    end
  end
end

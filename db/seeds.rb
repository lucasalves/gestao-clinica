# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

covenants = Covenant.create([
  {
    :name         => 'GEAP',
    :company_name => 'GEAP AUTOGESTÃO EM SAÚDE',
    :cnpj         => '03.658.432/0001-82',
    :registry_ans => '32308-0',
    :situation    => true,
    :segmentation => 'Operadora médico-hospitalar',
    :status       => true
  },
  {
    :name         => 'CENTRAL NACIONAL UNIMED',
    :company_name => 'CENTRAL NACIONAL UNIMED - COOPERATIVA CENTRAL',
    :cnpj         => '02.812.468/0001-06',
    :registry_ans => '33967-9',
    :situation    => true,
    :segmentation => 'Operadora médico-hospitalar',
    :status       => true
  },
  {
    :name         => 'BRADESCO SAUDE S/A',
    :company_name => 'BRADESCO SAÚDE S/A',
    :cnpj         => '92.693.118/0001-60',
    :registry_ans => '00571-1',
    :situation    => true,
    :segmentation => 'Operadora médico-hospitalar',
    :status       => true
  }
])
p "#{covenants.length} Convenios Cadastrados"

company = Company.create({
  :name => 'ASSOCIAÇÃO PAULISTA P/ DESENV. DA MEDICINA - HOSPITAL SÃO PAULO',
  :cnpj => '61.699.567/0001-92'
})
p "Empresa Cadastrada"

registry = RegistrationCovenant.create({
  :datetime => DateTime.now,
  :status   => true,
  :covenant => covenants.first,
  :company  => company
})

user = User.create({
  :first_name => 'José',
  :last_name  => 'Costa',
  :username   => 'demo',
  :email      => 'demo@localhost.com',
  :password   => '123456789',
  :birth_date => Date.new(1992, 12, 18)
})
p "Usuário (email: '#{user.email}', password: '#{user.password}') cadastrado!"

app = Integration::App.create({
  :name             => 'Sistema de Gestão',
  :domain           => 'http://0.0.0.0:4000',
  :test_path        => 'api/v1/test',
  :capture_path     => '/api/v1/atendimentos',
  :update_path      => '/api/v1/atendimentos',
  :capture_interval => 120
})
p "App '#{app.name}' cadastrada!"